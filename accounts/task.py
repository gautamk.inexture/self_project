from celery import shared_task
from django.core.mail import send_mail, EmailMessage
from dcelery.settings import EMAIL_HOST_USER


@shared_task
def send_mail_func():
    """
    this function is used when it is called by worker or by celery_beat(scheduled)
    """
    send_mail(
        subject="Celery Testing",
        message="This is testing part",
        from_email=EMAIL_HOST_USER,
        recipient_list=['gautamkr1998@gmail.com'],
        fail_silently=True,
    )
    return "Done"

