from django.urls import path
from .views import register_request, home, login_request


urlpatterns = [
    path('register/', register_request,name='register'),
    path('login/', login_request, name='login'),
    path('accounts/register/home/', home, name='home')
]
